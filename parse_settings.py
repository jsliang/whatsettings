# -*- coding: utf-8 -*-

import re
import json

def remove_comments(input_str):
    """
    Return a string value of input_str with comments removed.
    Supporting comment format: `// ...` and `/* ... */`

    >>> input_str = '''
    ... // this
    ... // is
    ...     // a
    ... 	// comment
    ... {}
    ... '''
    >>> remove_comments(input_str)
    '{}'

    >>> input_str = '''
    ... /* this
    ...  * is
    ...  * a
    ...  * comment*/
    ... {}
    ... '''
    >>> remove_comments(input_str)
    '{}'

    >>> input_str = '''
    ... // This is
    ... //  a comment.
    ...
    ... /* This is
    ...  *  another comment.*/
    ... {}
    ... '''
    >>> remove_comments(input_str)
    '{}'

    >>> input_str = '''
    ... {
    ... "str": "value1", // inline comment 1
    ... "bool": true, /* inline comment 2 */
    ... "int": 123,
    ... "float": 123.45
    ... }
    ... '''
    >>> remove_comments(input_str)
    '{\\n"str": "value1", \\n"bool": true, \\n"int": 123,\\n"float": 123.45\\n}'
    """

    # for
    # // comments like this
    # (omitting new lines)
    input_str = re.sub(r'^\s*//.*$[\r|\n]+', '', input_str, 0, re.MULTILINE)

    # for // inline comments link this
    # (preserving new lines)
    input_str = re.sub(r'//.*', '', input_str)

    # for
    # /*
    # comments
    # like
    # this
    # */
    input_str = re.sub(r'/\*.*?\*/', '', input_str, 0, re.DOTALL)

    return input_str.strip()

def load_json(input_str):
    """
    >>> input_str = '''
    ... {
    ... "str": "value1", // inline comment 1
    ... "bool": true, /* inline comment 2 */
    ... "int": 123,
    ... "float": 123.45
    ... }
    ... '''
    >>> json.loads( remove_comments(input_str) )
    {u'int': 123, u'float': 123.45, u'bool': True, u'str': u'value1'}

    >>> input_str = '''
    ... // While you can edit this file, it's best to put your changes in
    ... // "User/Preferences.sublime-settings", which overrides the settings in here.
    ... //
    ... // Settings may also be placed in file type specific options files, for
    ... // example, in Packages/Python/Python.sublime-settings for python files.
    ... {
    ... // Sets the colors used within the text area
    ... "color_scheme": "Packages/Color Scheme - Default/Monokai.tmTheme",
    ...
    ... // Note that the font_face and font_size are overriden in the platform
    ... // specific settings file, for example, "Preferences (Linux).sublime-settings".
    ... // Because of this, setting them here will have no effect: you must set them
    ... // in your User File Preferences.
    ... "font_face": "",
    ... "font_size": 10,
    ...
    ... // Valid options are "no_bold", "no_italic", "no_antialias", "gray_antialias",
    ... // "subpixel_antialias", "no_round" (OS X only) and "directwrite" (Windows only)
    ... "font_options": []
    ... }
    ... '''
    >>> json.loads( remove_comments(input_str) )
    {u'color_scheme': u'Packages/Color Scheme - Default/Monokai.tmTheme', u'font_face': u'', u'font_size': 10, u'font_options': []}
    """
    return json.loads(input_str)

if __name__ == "__main__":
    import doctest
    doctest.testmod()
